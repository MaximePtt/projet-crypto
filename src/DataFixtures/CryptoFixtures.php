<?php
namespace App\DataFixtures;

use App\Entity\Crypto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CryptoFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager) : void
    {
        $crypto1 = new Crypto();
        $crypto1->setName("bitcoin");
        $crypto1->setPrix("30000");
        $crypto1->setImgUrl("https://cryptologos.cc/logos/bitcoin-btc-logo.png");
        $manager->persist($crypto1);

        $crypto2 = new Crypto();
        $crypto2->setName("ETH");
        $crypto2->setPrix("2000");
        $crypto2->setImgUrl("https://cryptologos.cc/logos/ethereum-eth-logo.png");
        $manager->persist($crypto2);

        $manager->flush();
    }
}