<?php

namespace App\Entity;

use App\Repository\CommentaireRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentaireRepository::class)
 */
class Commentaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="commentaires")
     */
    private $commentaire_user;

    /**
     * @ORM\ManyToOne(targetEntity=Crypto::class, inversedBy="commentaires")
     */
    private $commentaire_crypto;

    public function __construct($description, $user, $crypto)
    {
        $this->description = $description;
        $this->commentaire_user = $user;
        $this->commentaire_crypto = $crypto;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCommentaireUser(): ?User
    {
        return $this->commentaire_user;
    }

    public function setCommentaireUser(?User $commentaire_user): self
    {
        $this->commentaire_user = $commentaire_user;

        return $this;
    }

    public function getCommentaireCrypto(): ?Crypto
    {
        return $this->commentaire_crypto;
    }

    public function setCommentaireCrypto(?Crypto $commentaire_crypto): self
    {
        $this->commentaire_crypto = $commentaire_crypto;

        return $this;
    }
}
