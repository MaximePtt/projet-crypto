<?php

namespace App\Entity;

use App\Repository\CryptoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CryptoRepository::class)
 */
class Crypto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imgUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="user_crypto_create")
     */
    private $creator;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="user_crypto_favoris")
     */
    private $users_have_favoris;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="commentaire_crypto")
     */
    private $commentaires;

    public function __construct()
    {
        $this->users_have_favoris = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getImgUrl(): ?string
    {
        return $this->imgUrl;
    }

    public function setImgUrl(?string $imgUrl): self
    {
        $this->imgUrl = $imgUrl;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getcreator(): ?User
    {
        return $this->creator;
    }

    public function setcreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsersHaveFavoris(): Collection
    {
        return $this->users_have_favoris;
    }

    public function addUsersHaveFavori(User $usersHaveFavori): self
    {
        if (!$this->users_have_favoris->contains($usersHaveFavori)) {
            $this->users_have_favoris[] = $usersHaveFavori;
            $usersHaveFavori->addUserCryptoFavori($this);
        }

        return $this;
    }

    public function removeUsersHaveFavori(User $usersHaveFavori): self
    {
        if ($this->users_have_favoris->removeElement($usersHaveFavori)) {
            $usersHaveFavori->removeUserCryptoFavori($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Commentaire>
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setCommentaireCrypto($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->removeElement($commentaire)) {
            // set the owning side to null (unless already changed)
            if ($commentaire->getCommentaireCrypto() === $this) {
                $commentaire->setCommentaireCrypto(null);
            }
        }

        return $this;
    }
}
