<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Entity()
 * @ORM\Table(name="user")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements \Symfony\Component\Security\Core\User\UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=Crypto::class, mappedBy="creator")
     */
    private $user_crypto_create;

    /**
     * @ORM\ManyToMany(targetEntity=Crypto::class, inversedBy="users_have_favoris")
     */
    private $user_crypto_favoris;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="commentaire_user")
     */
    private $commentaires;


    public function __construct()
    {
        $this->user_crypto_create = new ArrayCollection();
        $this->user_crypto_favoris = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->date_creation = new \DateTime();
        $this->date_modification = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->date_modification = new \DateTime();
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return Collection<int, Crypto>
     */
    public function getUserCryptoCreate(): Collection
    {
        return $this->user_crypto_create;
    }

    public function addUserCryptoCreate(Crypto $userCryptoCreate): self
    {
        if (!$this->user_crypto_create->contains($userCryptoCreate)) {
            $this->user_crypto_create[] = $userCryptoCreate;
            $userCryptoCreate->setCreator($this);
        }

        return $this;
    }

    public function removeUserCryptoCreate(Crypto $userCryptoCreate): self
    {
        if ($this->user_crypto_create->removeElement($userCryptoCreate)) {
            // set the owning side to null (unless already changed)
            if ($userCryptoCreate->getCreator() === $this) {
                $userCryptoCreate->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Crypto>
     */
    public function getUserCryptoFavoris(): Collection
    {
        return $this->user_crypto_favoris;
    }

    public function addUserCryptoFavori(Crypto $userCryptoFavori): self
    {
        if (!$this->user_crypto_favoris->contains($userCryptoFavori)) {
            $this->user_crypto_favoris[] = $userCryptoFavori;
        }

        return $this;
    }

    public function removeUserCryptoFavori(Crypto $userCryptoFavori): self
    {
        $this->user_crypto_favoris->removeElement($userCryptoFavori);

        return $this;
    }

    /**
     * @return Collection<int, Commentaire>
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setCommentaireUser($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->removeElement($commentaire)) {
            // set the owning side to null (unless already changed)
            if ($commentaire->getCommentaireUser() === $this) {
                $commentaire->setCommentaireUser(null);
            }
        }

        return $this;
    }


}
