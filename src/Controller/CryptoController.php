<?php

namespace App\Controller;

use App\Entity\Crypto;
use App\Entity\Commentaire;
use App\Form\CryptoType;
use App\Form\CommentaireType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Require ROLE_USER for *every* controller method in this class.
 *
 * @Route("{_locale}/")
 * @IsGranted("ROLE_USER")
 */ 

class CryptoController extends AbstractController
{
    /**
     * Lister tous les crypto.
     * @Route("crypto", name="crypto.list")
     * @return Response
     */
    public function list(UserInterface $user = null) : Response
    {
        $cryptos = $this->getDoctrine()->getRepository(Crypto::class)->findAll();

        $cryptosfav = $user->getUserCryptoFavoris();

        return $this->render('crypto/list.html.twig', [
            'cryptos' => $cryptos,
            'cryptosfav' => $cryptosfav
        ]);
    }

    /**
     * Lister tous les crypto favorites.
     * @Route("cryptofav", name="cryptofav.list")
     * @return Response
     */
    public function listfav(UserInterface $user = null) : Response
    {
        $cryptosfav = $user->getUserCryptoFavoris();

        return $this->render('crypto/favlist.html.twig', [
            'cryptosfav' => $cryptosfav
        ]);
    }

    /**
     * Lister tous les crypto favorites.
     * @Route("cryptocreated", name="cryptocreated.list")
     * @return Response
     */
    public function listcreated(UserInterface $user = null) : Response
    {
        $cryptoscreated = $user->getUserCryptoCreate();
        $cryptosfav = $user->getUserCryptoFavoris();

        return $this->render('crypto/createdlist.html.twig', [
            'cryptoscreated' => $cryptoscreated,
            'cryptosfav' => $cryptosfav
        ]);
    }

    /**
     * Chercher et afficher une crypto.
     * @Route("crypto/{id}", name="crypto.show", requirements={"id" = "\d+"})
     * @param Crypto $stage
     * @return Response
     */
    public function show(Crypto $crypto, UserInterface $user = null) : Response
    {
        $faving = $user->getUserCryptoFavoris()->contains($crypto);
        return $this->render('crypto/show.html.twig', [
            'crypto' => $crypto,
            'faving' => $faving
        ]);
    }

    /**
     * Créer une nouvelle crypto.
     * @Route("new-crypto", name="crypto.create")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     */
    public function create(Request $request, EntityManagerInterface $em, UserInterface $user = null) : Response
    {
        $crypto = new Crypto();
        $form = $this->createForm(CryptoType::class, $crypto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $crypto->setcreator($user);
            $em->persist($crypto);
            $em->flush();
            return $this->redirectToRoute('crypto.list');
        }
        return $this->render('crypto/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Éditer une crypto existante.
     * @Route("crypto/{id}/edit", name="crypto.edit")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     */
    public function edit(Request $request, Crypto $crypto, EntityManagerInterface $em) : Response
    {
        $form = $this->createForm(CryptoType::class, $crypto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('crypto.list');
        }
        return $this->render('crypto/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Supprimer un stage.
     * @Route("crypto/{id}/delete", name="crypto.delete")
     * @param Request $request
     * @param Crypto $crypto
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete(Request $request, Crypto $crypto, EntityManagerInterface $em) : Response
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('crypto.delete', ['id' => $crypto->getId()]))
            ->getForm();
        $form->handleRequest($request);
        if ( ! $form->isSubmitted() || ! $form->isValid()) {
            return $this->render('crypto/delete.html.twig', [
                'crypto' => $crypto,
                'form' => $form->createView(),
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($crypto);
        $em->flush();
        return $this->redirectToRoute('crypto.list');
    }

    /**
     * Ajouter crypto en favoris d'un user.
     * @Route("fav/{id}", name="crypto.fav")
     * @return Response
     */
    public function fav(Request $request, Crypto $crypto, EntityManagerInterface $em, UserInterface $user = null) : Response
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('crypto.fav', ['id' => $crypto->getId()]))
            ->getForm();
        $form->handleRequest($request);
        $crypto->addUsersHaveFavori($user);
        if ( ! $form->isSubmitted() || ! $form->isValid()) {
            return $this->render('crypto/fav.html.twig', [
                'crypto' => $crypto,
                'form' => $form->createView(),
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return $this->redirectToRoute('crypto.show', ['id' => $crypto->getId()]);
    }

    /**
     * Ajouter crypto en favoris d'un user.
     * @Route("unfav/{id}", name="crypto.unfav")
     * @return Response
     */
    public function unfav(Request $request, Crypto $crypto, EntityManagerInterface $em, UserInterface $user = null) : Response
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('crypto.unfav', ['id' => $crypto->getId()]))
            ->getForm();
        $form->handleRequest($request);
        $crypto->removeUsersHaveFavori($user);
        if ( ! $form->isSubmitted() || ! $form->isValid()) {
            return $this->render('crypto/unfav.html.twig', [
                'crypto' => $crypto,
                'form' => $form->createView(),
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return $this->redirectToRoute('crypto.show', ['id' => $crypto->getId()]);
    }


    /**
     * Ajouter crypto en favoris d'un user.
     * @Route("commenter/{id}", name="crypto.commenter")
     * @return Response
     */
    public function commenter(Request $request, Crypto $crypto, EntityManagerInterface $em, UserInterface $user = null) : Response
    {
        $form = $this->createForm(CommentaireType::class);
        $form->handleRequest($request);
        $crypto->removeUsersHaveFavori($user);
        if ( ! $form->isSubmitted() || ! $form->isValid()) {
            return $this->render('crypto/commentaire.html.twig', [
                'crypto' => $crypto,
                'form' => $form->createView(),
            ]);
        }
        $commentaire = new Commentaire("blabla", $user, $crypto);
        
        $em->clear();
        $user->addCommentaire($commentaire);
        $crypto->addCommentaire($commentaire);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return $this->redirectToRoute('crypto.show', ['id' => $crypto->getId()]);
    }

}
